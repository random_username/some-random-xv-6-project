//#define ATOMIC_SEMAPHORE
#define LOCK_SEMAPHORE
// Semephore with locks

#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "spinlock.h"
#include "proc.h"
#include "semaphore.h"

struct sem_t sem_list[NSEM];



// mark all semaphores as unused
// should be called inside the initialization of the kernel
void
sem_global_init()
{
  
  for (int i = 0; i < NSEM; i++)
  {
  // init the locks inside the semaphore vector if using locks
    initlock(&sem_list[i].lk, "");
    sem_list[i].used = 0;
  }
}

// create the semaphore and return its pointer
// should be called only once with a nonzero init
// each semaphore is uniquely identified with its index (not name as in POSIX)
struct sem_t*
sem_open(int index, int init, int value)
{
  if (init) {
    acquire(&(sem_list[index].lk));
    if (sem_list[index].used) {
      panic("try to initialize a used semaphore");
    }
    sem_list[index].value = value;
    sem_list[index].used = 1;
    release(&(sem_list[index].lk));
    return &(sem_list[index]);
  } else {
    return &(sem_list[index]);
  }
}

// try to create a new semaphore with the specified value
// return a pointer to the semaphore if succeeded
// return 0 if failed
struct sem_t*
sem_new(int value)
{
  int used = 0;
  struct sem_t* ret = 0;
  for (int i = 0; i < NSEM; i++)
  {
    acquire(&(sem_list[i].lk));
    used = sem_list[i].used;
    if (!used) {
      sem_list[i].value = value;
      sem_list[i].used = 1;
    }
    release(&(sem_list[i].lk));
    if (!used) {
      ret = &(sem_list[i]);
      break;
    }
  }
  return ret;
  
}

// wait on the semaphore
// return 0 if success
// return -1 if killed while waiting
// return -2 if the semaphore is freed
// yield the CPU if blocked
int
sem_wait(struct sem_t *sem)
{
  struct proc *p = myproc();
  acquire(&(sem->lk));
  while (1)
  {
    if (p->killed) {
      release(&(sem->lk));
      return -1;
    }
    if (sem->used == 0) {
      release(&(sem->lk));
      return -2;
    }
    if (sem->value > 0) {
      sem->value--;
      break;
    } else {
      sleep(sem, &(sem->lk));
    }
  }
  release(&(sem->lk));
  return 0;
}

// post on the semaphore, wake up everyone waiting on it
int 
sem_post(struct sem_t* sem)
{
  acquire(&(sem->lk));
  sem->value++;
  release(&(sem->lk));
  wakeup(sem);
  return 0; 
}

// try to wait on the semaphore
// return 0 if the wait should block
// return 1 if successfully waited
// this function never blocks
int
sem_trywait(struct sem_t* sem)
{
  acquire(&(sem->lk));
  if (sem->value > 0) {
    sem->value--;
    release(&(sem->lk));
    return 1;
  } else {
    release(&(sem->lk));
    return 0;
  }
}

// free the semaphore for others to use
// it is the CALLER's duty to ensure it won't be used again
// this function will wakeup everyone currently waiting on the semaphore
void
sem_free(struct sem_t* sem)
{
  acquire(&(sem->lk));
  sem->value = 0;
  sem->used = 0;
  release(&(sem->lk));
  wakeup(sem);
}

