
// Semaphore
struct sem_t {
  volatile int value;       // The value of the semaphore
#ifdef LOCK_SEMAPHORE
  struct spinlock lk; // spinlock protecting this semaphore
#endif
  volatile uint used;
  
};