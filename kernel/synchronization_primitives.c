#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "spinlock.h"
#include "proc.h"

#include "synchronization_primitives.h"

struct lightswitch_t lightswitch_list[NLIGHTSWITCH];

// mark all primitives as unused
// should be called inside the initialization of the kernel
void
primitive_init()
{
  for (uint i = 0; i < NLIGHTSWITCH; i++)
  {
    lightswitch_list[i].used = 0;
    lightswitch_list[i]._lock_value_sem = sem_new(1);
  }
}

/*
 * SIGNAL
 */

// initialize a signal
// return the pointer if succeeded
// return 0 if failed
struct signal_t*
signal_new()
{ 
  struct sem_t* sem = 0;
  sem = sem_new(0);
  return (struct signal_t*)sem;
}

// post on the signal, enable one waiting process to proceed
void
signal_post(struct signal_t* signal)
{
  sem_post(&(signal->_sem));
}

// wait on the signal, proceed if signal has been sent
// otherwise block until someone sends the signal
void
signal_wait(struct signal_t* signal)
{
  sem_wait(&(signal->_sem));
}

// destroy the signal, should be called if the signal will never be used
void
signal_free(struct signal_t* signal)
{
  sem_free(&(signal->_sem));
}

/*
 * TURNSTILE
 */

// initialize a turnstile, it will be initialized locked
// return the pointer if succeeded
// return 0 if failed
struct turnstile_t*
turnstile_new()
{
  struct sem_t *sem = 0;
  sem = sem_new(0);
  return (struct turnstile_t*)sem;
}

// enable the turnstile, allow all processes to proceed, including those not arrived yet
void
turnstile_enable(struct turnstile_t* turnstile)
{
  sem_post(&(turnstile->_sem));
}

// enter the turnstile, proceed if turnstile has been enabled
// otherwise yield the cpu until it is enabled
void
turnstile_enter(struct turnstile_t* turnstile)
{
  sem_wait(&(turnstile->_sem));
  sem_post(&(turnstile->_sem));
}

// disable the turnstile and block any process waiting for it
void
turnstile_disable(struct turnstile_t* turnstile)
{
  sem_wait(&(turnstile->_sem));
}

// free the turnstile, should be called if the turnstile will never be used
void
turnstile_free(struct turnstile_t* turnstile)
{
  sem_free(&(turnstile->_sem));
}

/*
 * RENDEZVOUS
 */

// initialize a rendezvous, must specify the number of threads using the rendezvous
// return the pointer if succeeded
// return 0 if failed
struct rendezvous_t*
rendezvous_new(uint num_threads)
{
  struct sem_t *sem = 0;
  sem = sem_new(1 - num_threads);
  return (struct rendezvous_t*)sem;
}

// enter the rendezvous, wait here if not enough threads have come to the point
void
rendezvous_wait(struct rendezvous_t* rendezvous)
{
  sem_post(&(rendezvous->_sem));
  sem_wait(&(rendezvous->_sem));
  sem_post(&(rendezvous->_sem));
}

// free the rendezvous, should be called if the rendezvous will never be used
void
rendezvous_free(struct rendezvous_t* rendezvous)
{
  sem_free(&(rendezvous->_sem));
}

/*
 * MULTIPLEX
 */


// initialize a rendezvous, should be called before it is used
// must specify the number of threads using the rendezvous
// return the pointer if succeeded
// return 0 if failed
struct multiplex_t*
multiplex_new(uint num_threads)
{
  struct sem_t *sem = 0;
  sem = sem_new(num_threads);
  return (struct multiplex_t*)sem;
}

// wait on the multiplex (try to occupy the resource)
// block if this should exceed the max cap specified
void
multiplex_wait(struct multiplex_t* multiplex)
{
  sem_wait(&(multiplex->_sem));
}

// post on the multiplex (free the resource used)
void
multiplex_post(struct multiplex_t* multiplex)
{
  sem_post(&(multiplex->_sem));
}

// free the multiplex, should be called if the multiplex will never be used
void
multiplex_free(struct multiplex_t* multiplex)
{
  sem_free(&(multiplex->_sem));
}

/*
 * LIGHTSWITCH
 */

// initialize a lightswitch, the lightswitch is inited opened
// return the pointer if succeeded
// return 0 if failed
struct lightswitch_t*
lightswitch_new()
{
  for (uint i = 0; i < NLIGHTSWITCH; i++)
  {
    if (sem_trywait(lightswitch_list[i]._lock_value_sem)) {
      if (lightswitch_list[i].used == 0) { // found an unused lighswitch in pool
        lightswitch_list[i].used = 1;
        lightswitch_list[i]._value = 0;
        sem_post(lightswitch_list[i]._lock_value_sem);
        lightswitch_list[i]._allow_acquire_sem = sem_new(1);
        if (lightswitch_list[i]._allow_acquire_sem == 0) {
          sem_wait(lightswitch_list[i]._lock_value_sem);
          lightswitch_list[i].used = 0;
          sem_post(lightswitch_list[i]._lock_value_sem);
          return 0; // sem used up, don't expect to init a lightswitch
        }
        return &(lightswitch_list[i]);
      } else {
        sem_post(lightswitch_list[i]._lock_value_sem);
      }
    }
  }
  return 0;
}

// enter the lightswitch as a first-type thread
// block iff there is a second-type thread occupying the lightswitch
void
lightswitch_enter(struct lightswitch_t* lightswitch)
{
  sem_wait(lightswitch->_lock_value_sem);
  lightswitch->_value++;
  if (lightswitch->_value == 1) {  // the first one entered blocks future acquiring
    sem_wait(lightswitch->_allow_acquire_sem);
    sem_post(lightswitch->_lock_value_sem); 
  } else {
    sem_post(lightswitch->_lock_value_sem);
  }
  
}

// leave the lightswitch as a first-type thread
void
lightswitch_leave(struct lightswitch_t* lightswitch)
{
  sem_wait(lightswitch->_lock_value_sem);
  lightswitch->_value--;
  if (lightswitch->_value == 0) {
    sem_post(lightswitch->_lock_value_sem);
    sem_post(lightswitch->_allow_acquire_sem);
  } else {
    sem_post(lightswitch->_lock_value_sem);
  }
}

// acquire the lightswitch as a second-type thread
// block if any thread is using the lightswitch
void
lightswitch_acquire(struct lightswitch_t* lightswitch)
{
  sem_wait(lightswitch->_allow_acquire_sem);
}

// release the lightswitch as a second-type thread
// allow other threads to use this lightswitch
void
lightswitch_release(struct lightswitch_t* lightswitch)
{
  sem_post(lightswitch->_allow_acquire_sem);
}

// free the lightswitch, should be called if the lightswitch will never be used
void
lightswitch_free(struct lightswitch_t* lightswitch)
{
  sem_free(lightswitch->_allow_acquire_sem);
  sem_free(lightswitch->_lock_value_sem);
  lightswitch->_lock_value_sem = sem_new(1);
  lightswitch->_value = 0;
}