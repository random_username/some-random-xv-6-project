#include "defs.h"
#include "semaphore.h"


// signaling, used for signal passing
// it ensures that the receiver could only proceed with the signal posted
// either the signal has been posted at the time the receiver waits
// or the receiver will have to wait until it is posted
struct signal_t
{
  struct sem_t _sem;
};

// turnstile, mostly used to indicate a initialization
// once a turnstile is enabled, any thread can enter is allowed
// when disabled, no thread shall pass the point
struct turnstile_t
{
  struct sem_t _sem;
};

// rendezvous, used for synchronizing
// it ensures all threads meet a common point before proceeding
// either all other threads have come to the point in their own execution
// or the thread will have to wait for the remaining threads
// 
// the rendezvous should only be used ONCE, after which it should be freed
struct rendezvous_t
{
  struct sem_t _sem;
};

// multiplex, used to abstract a limited amount of a certain resource
// at any time, no more than the spcified amount of threads can grab the resource
struct multiplex_t
{
  struct sem_t _sem;
};

// lightswitch, used for modeling two diffrent types of threads accessing a common resource
// a first-type thread allows other first-type threads but blocks any second-type thread
// a second-type thread blocks every other thread from accessing the resource
struct lightswitch_t
{
  struct sem_t *_allow_acquire_sem;  // lock of entering
  struct sem_t *_lock_value_sem;  // lock to protect self->_value and self->used
  volatile int _value; // the amount of threads currently in the lightswitch
  volatile uint used;
};