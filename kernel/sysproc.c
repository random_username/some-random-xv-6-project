#include "types.h"
#include "riscv.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "spinlock.h"
#include "proc.h"

uint64
sys_exit(void)
{
  int n;
  if(argint(0, &n) < 0)
    return -1;
  exit(n);
  return 0;  // not reached
}

uint64
sys_getpid(void)
{
  return myproc()->pid;
}

uint64
sys_fork(void)
{
  return fork();
}

uint64
sys_wait(void)
{
  uint64 p;
  if(argaddr(0, &p) < 0)
    return -1;
  return wait(p);
}

uint64
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

uint64
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

uint64
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

// return how many clock tick interrupts have occurred
// since start.
uint64
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

// Custom test
uint64
sys_test(void)
{
  int loop_count = 1;
  struct rendezvous_t* ls;
  int para;
  uint64 pointer;
  argint(0, &para);
  argaddr(1, &pointer);
  if (pointer == 0) {
    ls = rendezvous_new(4);
    return (uint64)ls;
  }
  ls = (struct rendezvous_t*)pointer;
  // the order of char printed DOES NOT REPRESENT the order of execution
  // there could be a timer interrupt in the middle of the print and the previous / next instruction
  // or simply two proc on different cpu are racing the console
  // all of those will mess up the order
  // however, it does SUGGEST the very order
  // as timer interrupt is relatively scarce, and racing does little messing
  switch (para)
  {
  case 0:
    for (int i = 0; i < loop_count; i++)
    {
      printf("a");
      rendezvous_wait(ls);
      printf("A");
      // rendezvous_post(ls);
      // printf("a");
    }
    break;
  case 1:
    for (int i = 0; i < loop_count; i++)
    {
      printf("b");
      rendezvous_wait(ls);
      printf("B");
      // rendezvous_post(ls);
      // printf("b");
    }
    break;
  case 2:
    for (int i = 0; i < loop_count; i++)
    {
      printf("c");
      rendezvous_wait(ls);
      printf("C");
      // rendezvous_post(ls);
      // printf("c");
    }
    break;
  case 3:
    for (int i = 0; i < loop_count; i++)
    {
      printf("d");
      rendezvous_wait(ls);
      printf("D");
      // rendezvous_post(ls);
      // printf("d");
    }
    break;
  default:
    break;
  }
  wait(0);
  wait(0);
  rendezvous_free(ls);
  return 0;
}