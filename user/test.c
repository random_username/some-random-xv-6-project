// a hook to test anything

#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"

int
main(int argc, char *argv[])
{
  uint64 primitive_pointer;
  primitive_pointer = test(0, 0);
  if (fork() == 0) {
    if (fork() == 0) {
      test(0,primitive_pointer);
    } else {
      test(1,primitive_pointer);
    }
  } else {
    if (fork() == 0) {
      test(2,primitive_pointer);
    } else {
      test(3,primitive_pointer);
    }
  }
  exit(0);
}
